package mascotas;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
public class Principal {
	private static String nombre = "";
	private static int edad = 0;
	private static String raza = "";
	private static int op = 0;
	static Mascota mascota = new Mascota();
	static ArrayList<Mascota> MascotasList = new ArrayList<Mascota>();

	public static void main(String[] args) {
		while(op!=3){
			op = menu();
			switch(op){
			case 1:
				mascota = new Mascota();
				mascota.setNombre(nombreMascota());
				mascota.setEdad(edadMascota());
				mascota.setRaza(razaMascota());
				MascotasList.add(mascota);
				break;
			case 2:
				mascota = new Mascota();
				muestraTodosAnimales();
				break;
			case 3:
				mascota = new Mascota();
				mascotaJoven();
				break;
			case 4:
				mascota = new Mascota();
				queryMascota();
				break;
			case 5:
				mascota = new Mascota();
				eliminaAnimales();
				break;
			default:
				System.out.println("Introduce un valor valido");
			}
		}
	}
	/**
	 * Menu para elegir una opcion
	 * @return la opcion del usuario
	 */
	public static int menu() {
		Scanner sc = new Scanner(System.in);
		System.out.println("1-Inserta animales");
		System.out.println("2-Muestra todos los animales");
		System.out.println("3-Muestra mascota más joven");
		System.out.println("4-Busca mascota con su raza");
		System.out.println("5-Elimina animales a partir de su nombre");
		return sc.nextInt();
	}
	
	/**
	 * Pedimos el nombre de la mascota
	 * @return nombre de la mascota
	 */
	public static String nombreMascota(){
		Scanner cs = new Scanner(System.in);
		System.out.println("Introduce nombre de la mascota");
		System.out.print(">");
		nombre = cs.nextLine();
		return nombre;
	}
	/**
	 * Pedimos la edad de la mascota
	 * @return devolvemos la edad de la mascota
	 */
	public static int edadMascota(){
		Scanner cs = new Scanner(System.in);
		System.out.println("Introduce edad de la mascota");
		System.out.print(">");
		edad = cs.nextInt();
		return edad;
	}
	/**
	 *Pedimos la raza de la mascota
	 * @return devolvemos la raza de la mascota
	 */
	public static String razaMascota(){
		Scanner cs = new Scanner(System.in);
		System.out.println("Introduce raza de la mascota");
		System.out.print(">");
		raza = cs.nextLine();
		return raza;
	}
	
	/*
	 * Query para buscar la mascota más joven
	 */
	public static void mascotaJoven(){
		String nombreMascotaJoven = "";
		int edadMascotaJoven = 20;
		
		for (Mascota mascota:MascotasList){
			if(mascota.getEdad()<edadMascotaJoven){
				edadMascotaJoven = mascota.getEdad();
				nombreMascotaJoven = mascota.getNombre();
			}
		}
		System.out.println("La mascota más joven se llama: "+nombreMascotaJoven +" y tiene "+edadMascotaJoven);
	}
	
	/*
	 * Query para buscar una mascota a partir de la raza
	 */
	public static void queryMascota(){
		Scanner sc = new Scanner (System.in);
		System.out.println("Raza mascota: ");
		String razaMascota = sc.nextLine();
		int contador = 0;
		for(Mascota mascota:MascotasList){
			if(mascota.getRaza().equalsIgnoreCase(razaMascota)){
				contador++;
				System.out.println(
						"Resultados encontrados con la raza: "+razaMascota+"\n"+
						"\n Nombre: "+mascota.getNombre()+
						"\n Edad: "+mascota.getEdad()+
						"\n Raza: "+mascota.getRaza()
				);
			}
			if(contador==0){
				System.out.println("No hay mascotas con la raza indicada");
			}
		}
	}
	/*
	 * Muestra todos los animales que hay el arrayList
	 */
	public static void muestraTodosAnimales(){
		for(Mascota mascota:MascotasList){
			System.out.println(
					"\n Nombre: "+mascota.getNombre()+
					"\n Edad: "+mascota.getEdad()+
					"\n Raza: "+mascota.getRaza()
			);
	
		}
	}
	
	/*
	 * Elimina animales a partir de su nombre
	 */
	public static void eliminaAnimales(){
		Scanner sc = new Scanner(System.in);
		System.out.print("Nombre mascota:");
		String nombreIndicado = sc.nextLine();
		Iterator<Mascota> it = MascotasList.iterator();
		while (it.hasNext()) {
		  Mascota mascota = it.next();
		  if (mascota.getNombre().equalsIgnoreCase(nombreIndicado)) {
		    it.remove();
		  }
		}
	}
}
